xml.instruct!
xml.feed "xmlns" => "http://www.w3.org/2005/Atom" do
  site_url = 'https://peertube_isolation.frama.io/'
  xml.title 'PeerTube Isolation'
  xml.subtitle 'Organizing to block Far right\'s PeerTube instance from the Fediverse and isolate them from us.'
  xml.id site_url
  xml.link "href" => URI.join(site_url, current_page.path), "rel" => "self"
  xml.updated(Date.parse(data.instances.to_a.last[1].added).to_time.iso8601)
  xml.author { xml.name 'PeerTube Isolation' }

  data.instances.each do |instance|
    xml.entry do
      xml.title instance[0]
      xml.id URI.join(site_url, '#' + instance[0])
      xml.published Date.parse(instance[1].added).to_time.iso8601
      xml.updated Date.parse(instance[1].added).to_time.iso8601
      xml.author { xml.name 'PeerTube Isolation' }
      xml.content instance[1].comment, "type" => "text"
    end
  end
end

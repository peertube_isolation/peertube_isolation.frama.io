---
title: PeerTube Isolation 
---

# PeerTube Isolation 

Organizing to block Far right's PeerTube instance from the Fediverse and isolate them from us.

**The list is available here: [/list](/list)**

On this list you can find instances dedicated to hate content as well as unmoderated instances used for that purpose.

## Contribute
You can help by reporting instances.

On framagit.org (account required): [New Report](https://framagit.org/peertube_isolation/peertube_isolation.frama.io/-/issues/new?issuable_template=Report).


## Automute plugin for PeerTube
The automute plugin for PeerTube use a json list to mute instances or account.

- [Official documentation of the plugin](https://framagit.org/framasoft/peertube/official-plugins/-/blob/master/peertube-plugin-auto-mute/README.md).

- [PeerTube Isolation in JSON](/list/peertube_isolation.json). You can follow it as you would follow the mute list of a PeerTube instance.


## Raw list
A raw list of URLs is available here: [/list/peertube_isolation.txt](/list/peertube_isolation.txt).

For example you can use it on uBlock Origin (Settings > Filters lists > Custom) or on PiHole.

## Stay updated
You can subscribe to the RSS feed: [/list/feed.xml](/list/feed.xml)

~~You can follow <a href="https://cornichon.me/@PeerTube_Isolation" rel="me">@PeerTube_Isolation@cornichon.me</a> on the fediverse.~~ It is no longer active.


## Credit & reusage
You are free to use this list as a source for larger blocklists. Attribution is appreciated but not mandatory.
